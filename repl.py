# -*- coding: utf-8 -*-
"""
Created on Wed Oct 22 21:01:35 2014

@author: anzel
"""

import sys

import game, player


def main(**args):
    print "Initializing game..."
    g = game.Game()
    p = player.Player(g)

    command = raw_input("scrabble% ")
    while command:
        eval(command)


if __name__ == "__main__": main()
