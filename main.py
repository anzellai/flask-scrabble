# -*- coding: utf-8 -*-
"""
Created on Wed Oct 22 21:01:35 2014

@author: anzel
"""

from flask import Flask
from flask import jsonify
from pymongo import Connection
from board import Board
from leave import Leave
import threading


DEBUG = False
LOGGING = True

def printout(output):
    if LOGGING:
        print "{}".format(output)
        return
    return

if not DEBUG:
    # in order for gunicorn to work
    from werkzeug.contrib.fixers import ProxyFix

    lock = threading.Lock()
    if 'b' not in locals():
        printout("Initializing dictionary, please wait...")
        b = Board()
        printout("Dictionary loaded.")
        leave = Leave
    conn = Connection('localhost', 27017)
    db = conn['msa-apps']
    spectatorId = db.users.find_one({'username': 'Computer'})['_id']
    game_queue = []


app = Flask(__name__)
app.debug = not DEBUG
app.threaded = True

@app.errorhandler(404)
def page_not_found(error):
    return "Page not found", 404


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/api/<gameId>', methods=['GET', 'POST'])
def play_ai(gameId):
    with lock:
        global game_queue
        global db
        global spectatorId
        global leave

        printout(threading.current_thread())

        res_changetiles = {'result': 'changetiles'}
        res_challenge = {'result': 'challenge'}
        res_pass = {'result': 'pass'}
        res_rematch = {'result': 'rematch'}
        res_404 = {'result': '404'}
        res_300 = {'result': '300'}

        if not gameId:
            printout("Game not found")
            return jsonify(res_404)
        if DEBUG:
            if gameId in game_queue:
                printout("Game already in queue")
                return jsonify(res_300)
            else:
                game_queue.append(gameId)
            printout("GAME QUEUE => {}".format(game_queue))
        else:
            db_queue = db.queue.find_one({'gameId': gameId})
            if db_queue:
                printout("Game already in db_queue")
                return jsonify(res_300)
            else:
                db.queue.insert({'gameId': gameId})

        game = db.games.find_one(gameId)

        if not game:
            printout("Game not found")
            return jsonify(res_404)

        playerTurn = db.users.find_one(game["playerTurn"])
        printout("playerTurn => {}".format(playerTurn['username']))

        if 'Rematch' in game['winner']:
            printout("Computer accepts rematch request")
            if DEBUG:
                game_queue.remove(gameId)
            else:
                db.queue.remove({'gameId': gameId})
            return jsonify(res_rematch)

        if not game['playerTwoTiles']:
            printout("End of game")
            if DEBUG:
                game_queue.remove(gameId)
            else:
                db.queue.remove({'gameId': gameId})
            return jsonify(res_pass)

        if not game['playerOneTiles']:
            printout("End of game")
            if DEBUG:
                game_queue.remove(gameId)
            else:
                db.queue.remove({'gameId': gameId})
            return jsonify(res_pass)

        if game['playerTurn'] != spectatorId:
            printout("Not computer turn")
            if DEBUG:
                game_queue.remove(gameId)
            else:
                db.queue.remove({'gameId': gameId})
            return jsonify(res_300)

        if game['challenge']:
            lastWord = game['lastWord']
            if lastWord:
                for word in lastWord:
                    word = word.replace(" ", "").lower()
                    if "*" in word:
                        continue
                    check_word = db.dictionary.find_one({'word': word})
                    if not check_word:
                        print("Raise challenge => {}".format(word))
                        if DEBUG:
                            game_queue.remove(gameId)
                        else:
                            db.queue.remove({'gameId': gameId})
                        return jsonify(res_challenge)

        # generate a new board
        board = game['board']
        for by, iy in enumerate(board):
            for bx, ix in enumerate(board[by]):
                if '*' in board[by][bx]['content']:
                    printout("Unassigned blank tile found, queue removed and will retry in 3s")
                    if DEBUG:
                        game_queue.remove(gameId)
                    else:
                        db.queue.remove({'gameId': gameId})
                    return jsonify(res_challenge)
                if board[by][bx]['content'] != "":
                    b.grid[by][bx] = board[by][bx]['content'].strip().lower()
                else:
                    b.grid[by][bx] = '.'
        printout(b)
        rack = game['playerTwoTiles']
        if not rack:
            if DEBUG:
                game_queue.remove(gameId)
            else:
                db.queue.remove({'gameId': gameId})
            return jsonify(res_pass)
        tiles = ''.join([tile['letter'] for tile in rack]).replace('.', '_').replace('*', '_').lower()
        printout("tiles on hand: {}".format(tiles))

        try:
            if len(rack) < 7:
                bestmove = b.moves(tiles)[0]
            else:
                bestmove = b.moves(tiles)
                for i, move in enumerate(bestmove):
                    remaining = tiles
                    for letter in move[1]:
                        remaining = remaining.replace(letter, "", 1)
                    bestmove[i] = (
                        move[0], move[1], move[2], move[3], \
                        move[4] - 50 if len(move[1]) >= 7 and remaining != "" else move[4], \
                        remaining, \
                        (move[4] -50 if len(move[1]) >= 7 and remaining != "" else move[4]) \
                        + sum(leave[r] for r in remaining))
                bestmove.sort(key=lambda x: x[6], reverse=True)
                bestmove = bestmove[0]
            printout("best move => \n" + str(bestmove))
        except IndexError:
            printout("No move can be made... ")
            if len(game['bag']) == 0:
                if DEBUG:
                    game_queue.remove(gameId)
                else:
                    db.queue.remove({'gameId': gameId})
                return jsonify(res_pass)
            if DEBUG:
                game_queue.remove(gameId)
            else:
                db.queue.remove({'gameId': gameId})
            return jsonify(res_changetiles)
        playtiles = [besttile for besttile in bestmove[0]]
        playx = [bestx for bestx in range(bestmove[2][0], bestmove[3][0] + 1)]
        playy = [besty for besty in range(bestmove[2][1], bestmove[3][1] + 1)]
        # fill x,y to match na coords
        if len(playx) > len(playy):
            playy *= len(playx)
        elif len(playy) > len(playx):
            playx *= len(playy)
        playmove = zip(playtiles, playx, playy)
        printout("playmove => {}".format(playmove))
        playmovelist = \
            [{'x': x, 'y': y, 'v': v} for v, x, y in playmove if b.square(x, y) == "."]

        blanks = []
        for i, j in enumerate(playmovelist):
            if playmovelist[i]['v'] in tiles:
                tiles = tiles.replace(playmovelist[i]['v'], '', 1)
            elif playmovelist[i]['v'] not in tiles and '_' in tiles:
                tiles = tiles.replace('_', playmovelist[i]['v'], 1)
                blanks.append({
                    'x': playmovelist[i]['x'],
                    'y': playmovelist[i]['y'],
                    'v': playmovelist[i]['v']})
                playmovelist[i]['v'] = '*'
        if blanks:
            playmovedict = {'playmovedict': playmovelist, 'blanks': blanks}
        else:
            playmovedict = {'playmovedict': playmovelist}
        printout(playmovedict)
        b.play(bestmove[0], bestmove[2], bestmove[3])
        printout(b)
        printout("queue removed...")
        if DEBUG:
            game_queue.remove(gameId)
        else:
            db.queue.remove({'gameId': gameId})
        return jsonify(playmovedict)

if not DEBUG:
    queuesInDB = db.queue.find()
    if queuesInDB:
        for queueInDB in queuesInDB:
            printout("Resuming queue <= {}".format(queueInDB['gameId']))
            gameId = queueInDB['gameId']
            db.queue.remove({'gameId': gameId})
            try:
                play_ai(gameId)
            except:
                pass
    app.wsgi_app = ProxyFix(app.wsgi_app)


if __name__ == '__main__':
    if DEBUG:
        game_queue = []
        lock = threading.Lock()
        if 'b' not in locals():
            print "Initializing dictionary, please wait..."
            b = Board()
            print "Dictionary loaded."
            leave = Leave
        conn = Connection('localhost', 27017)
        db = conn['msa-apps']
        spectatorId = db.users.find_one({'username': 'Computer'})['_id']
        app.run(debug=True)

