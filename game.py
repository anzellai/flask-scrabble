# -*- coding: utf-8 -*-
"""
Created on Wed Oct 22 21:01:35 2014

@author: anzel
"""

from bag import *
from board import *


class GameError(Exception):
    pass


class Game:
    def __init__(self):
        self.bag = Bag()
        self.board = Board()

    def __repr__(self):
        # TODO: Output something a little more interesting.
        return repr(self.board)

    def draw(self, letters):
        # TODO: Make this transactional
        for l in letters:
            self.bag.draw(l)
        return letters

        # TODO: Make sure the rack has seven or less characters.

    def play(self, word, start, end):
        if isinstance(word, list) and \
                isinstance(start, list) and \
                isinstance(end, list) and \
                                len(word) == len(start) == len(end):
            for wo, st, en in word, start, end:
                try:
                    self.board.play(wo, st, en)
                except BoardError, e:
                    raise GameError(e)
        else:
            try:
                self.board.play(word, start, end)

            except BoardError, e:
                # TODO: Do something useful with this.
                raise GameError(e)
